const data = [{
    email: "abduh@mail.com",
    quiz1: 78,
    quiz2: 89,
    quiz3: 90
}, {
    email: "khairun@mail.com",
    quiz1: 95,
    quiz2: 85,
    quiz3: 88
}, {
    email: "bondra@mail.com",
    quiz1: 70,
    quiz2: 75,
    quiz3: 79
}, {
    email: "regi@mail.com",
    quiz1: 91,
    quiz2: 89,
    quiz3: 93
}];

const getAverage = (arr) => {
    const reducer = (total, currentObj) => total + currentObj.quiz1;
    const sum = arr.reduce(reducer, 0);
    return sum / arr.length;
}

const getAverage2 = (arr) => {
    const reducer = (total, currentObj) => total + currentObj.quiz2;
    const sum = arr.reduce(reducer, 0);
    return sum / arr.length;
}

const getAverage3 = (arr) => {
    const reducer = (total, currentObj) => total + currentObj.quiz3;
    const sum = arr.reduce(reducer, 0);
    return sum / arr.length;
}

console.log("Quiz1 : " + getAverage(data));
console.log("Quiz2 : " + getAverage2(data));
console.log("Quiz3 : " + getAverage3(data));
console.log("hasil rata-rata quiz semua adalah = " + getAverage(data) + getAverage2(data) + getAverage2(data));