// tugas 1
console.log('-----------------LOOPING PERTAMA-----------------')
var awal = 0;
var akhir = 20;
while (awal < akhir) {
    awal = awal + 2;
    console.log(awal + ' - I love Coding')
}

console.log('-----------------LOOPING KEDUA-----------------')
var awal = 22;
var akhir = 2;
while (awal > akhir) {
    awal = awal - 2;
    console.log(awal + ' - I love Coding')
}

// tugas 2
console.log('\n________________ Looping menggunakan for ________________ ')

for (var angka = 2; angka <= 20; angka++) {
    if (angka % 3 == 0) {
        console.log(angka + ' I love Coding')
    } else if (angka % 2 == 0) {
        console.log(angka + ' Berkualitas')
    } else if (angka) {
        console.log(angka + ' Santai')
    }
}

// TUGAS 3
console.log('\n________________ Membuat Persegi Panjang ________________ ')

var lebar = 8;
var panjang = 4;

for (var i = 0; i < panjang; i++) {
    var tmp = '';

    for (var j = 0; j < lebar; j++) {
        tmp += '#'
    }
    console.log(tmp);
}

// TUGAS 4
console.log('\n________________ TAG ________________ ')
var row = 5;

for (var i = 1; i <= row; i++) {
    var result = '';
    for (var j = 1; j <= i; j++) {

        result += '#';
    }
    console.log(result);
}

// tugas 5
console.log('\n________________ Membuat Papan Catur ________________ ')
for (var angka = 4; angka <= 8; angka++) {
    var result = ' ';
    if (angka % 2 == 0) {
        for (var j = 1; j <= i; j++) {

            result += ' # ';
        }
    } else if (angka) {
        for (var j = 1; j <= i; j++) {

            result += '  #';
        }
    }

    console.log(result);
}