// Tugas 1
var nama = 'Mahmudah'
var peran = "werewolf"

console.log('----------------Tugas 1----------------');
if (nama == '') {
    console.log('nama harus diisi')
} else if (nama && peran == '') {
    console.log('halo' + nama + ', pilih peranmu untuk memulai game')
} else if (nama == 'Jubaidah' && peran == 'penyihir') {
    console.log('Halo Penyihir Jubaidah, jubaidah dapat melihat siapa yang menjadi werewolf!')
} else if (nama == 'Mahmudah' && peran == 'werewolf') {
    console.log('Halo Werewolf mahmudah, Kamu akan memakan mangsa setiap malam!')
} else if (nama == 'Bambang' && peran == 'guard') {
    console.log('Halo Guard Bambang, kamu akan membantu melindungi temanmu dari serangan werewolf')
}

// Tugas 2
console.log('\n----------------Tugas 2----------------');
var tanggal = 4;
var bulan = 6;
var tahun = 1910;
var ketBulan;

switch (true) {
    case (tanggal < 1 || tanggal > 31):
        {
            console.log('input tanggal salah')
            break;
        }
    case (tahun < 1900 || tahun > 2200):
        {
            console.log('input tahun salah')
            break;
        }

    case (bulan > 12 || bulan < 1):
        console.log('input Bulan Salah')
        break;
    default:
        {
            switch (true) {
                case bulan == 1:
                    ketBulan = 'Januari';
                    break;
                case bulan == 2:
                    ketBulan = 'febuari';
                    break;
                case bulan == 3:
                    ketBulan = 'maret';
                    break;
                case bulan == 4:
                    ketBulan = 'april';
                    break;
                case bulan == 5:
                    ketBulan = 'mei';
                    break;
                case bulan == 6:
                    ketBulan = 'juni';
                    break;
                case bulan == 7:
                    ketBulan = 'juli';
                    break;
                case bulan == 8:
                    ketBulan = 'agustus';
                    break;
                case bulan == 9:
                    ketBulan = 'september';
                    break;
                case bulan == 10:
                    ketBulan = 'oktober';
                    break;
                case bulan == 11:
                    ketBulan = 'november';
                    break;
                case bulan == 12:
                    ketBulan = 'desember';
                    break;
                default:
                    break;
            }
        }

}

console.log(tanggal, ' ', ketBulan, ' ', tahun)