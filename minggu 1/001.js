function bandingkan(num1, num2) {
    if ((num1 && num2 < 0) || num1 < 0 || num2 < 0) {
        return -1;
    } else if (num1 < num2) {
        return num2;
    } else if (num1 > num2) {
        return num1;
    } else if (num1 === num2) {
        return -1;
    } else if ((num1 = 1)) {
        return 1;
    }
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121)); // 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")); // 18

function balikString(str) {
    var currentString = str;
    var newString = "";
    for (let i = str.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }

    return newString;
}
console.log(balikString("abcde")); // edcba
console.log(balikString("rusak")); // kasur
console.log(balikString("racecar")); // racecar
console.log(balikString("haji")); // ijah

function palindrome(str) {
    var re = /[^A-Za-z0-9]/g;
    str = str.toLowerCase().replace(re, "");
    var len = str.length;
    for (var i = 0; i < len / 2; i++) {
        if (str[i] !== str[len - 1 - i]) {
            return false;
        }
    }
    return true;
}
console.log(palindrome("kasur rusak")); // true
console.log(palindrome("haji ijah")); // true
console.log(palindrome("nabasan")); // false
console.log(palindrome("nababan")); // true
console.log(palindrome("jakarta")); // false