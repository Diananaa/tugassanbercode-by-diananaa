// Tugas 1
console.log('-------------Tugas 1-------------')

function teriak() {
    var kata = 'hallo Sanbercode';
    return kata
}
console.log(teriak());

// tugas 2
console.log('\n-------------Tugas 2-------------')

function kalikan(num1, num2) {
    return num1 * num2
}
var num1 = 12
var num2 = 4
var hasilkali = kalikan(num1, num2)
console.log(hasilkali)

// tugas 3
console.log('\n-------------Tugas 3-------------')

function keterangan(name, age, address, hobby) {
    var ket = 'Nama saya ' + name + ', umur saya ' + age + ' tahun, alamat saya di ' + address + ' dan saya mempunyai hobby yaitu ' + hobby
    return ket
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = keterangan(name, age, address, hobby)
console.log(perkenalan)